require 'test_helper'

class IdeaNodesControllerTest < ActionController::TestCase
  setup do
    @idea_node = idea_nodes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:idea_nodes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create idea_node" do
    assert_difference('IdeaNode.count') do
      post :create, :idea_node => { :dateCreated => @idea_node.dateCreated, :dateModified => @idea_node.dateModified, :description => @idea_node.description, :rating => @idea_node.rating, :rootId => @idea_node.rootId, :tags => @idea_node.tags, :title => @idea_node.title, :userId => @idea_node.userId }
    end

    assert_redirected_to idea_node_path(assigns(:idea_node))
  end

  test "should show idea_node" do
    get :show, :id => @idea_node
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @idea_node
    assert_response :success
  end

  test "should update idea_node" do
    put :update, :id => @idea_node, :idea_node => { :dateCreated => @idea_node.dateCreated, :dateModified => @idea_node.dateModified, :description => @idea_node.description, :rating => @idea_node.rating, :rootId => @idea_node.rootId, :tags => @idea_node.tags, :title => @idea_node.title, :userId => @idea_node.userId }
    assert_redirected_to idea_node_path(assigns(:idea_node))
  end

  test "should destroy idea_node" do
    assert_difference('IdeaNode.count', -1) do
      delete :destroy, :id => @idea_node
    end

    assert_redirected_to idea_nodes_path
  end
end
