class ResetTable < ActiveRecord::Migration
  def up
  	create_table "idea_nodes", :force => true do |t|
    t.integer  "rootId"
    t.string  "username"
    t.string   "title"
    t.text     "description"
    t.integer  "rating"
    t.string     "tag"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end
  end

  def down
  	drop_table :idea_nodes
  end
end
