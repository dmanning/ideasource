class AddUserpointsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :userpoints, :integer
  end
  add_index :users, :username, :unique => true
end
