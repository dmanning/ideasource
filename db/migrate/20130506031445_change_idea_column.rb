class ChangeIdeaColumn < ActiveRecord::Migration
  def up
  	create_table "idea_nodes", :force => true do |t|
    t.integer  "rootId"
    t.integer  "parentId"
    t.string   "username"
    t.string   "title"
    t.text     "description"
    t.integer  "rating"
    t.string   "tag"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end
  create_table "tags", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end
  end

  def down
  	drop_table :idea_nodes
    drop_table :tags
  end
end
