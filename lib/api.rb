require 'net/http'
 
class Api
  attr_accessor :url
  attr_accessor :uri
  
  def initialize
    @url = "http://localhost:3000/idea_nodes"
    @uri = URI.parse @url
  end
  
  # Create an IdeaNode using a predefined XML template as a REST request.
  def create(rootID=0,parentId=0,title="",description="",username="",tag="")
    if title == "" || description == "" || username == "" || tag == ""
      puts "invalid input"
    else
      xml_req =
    "<?xml version='1.0' encoding='UTF-8'?>
    <IdeaNode>
      <rootId type='integer'>#{rootId}</rootId>
      <parentId type='integer'>#{parentId}</parentId>
      <title>#{title}</title>
      <description>#{description}</description>
      <username>#{username}</username>
      <tag>#{tag}</tag>
      <rating type='integer'>0</rating>
    </IdeaNode>"
    
    request = Net::HTTP::Post.new(@url)
    request.add_field "Content-Type", "application/xml"
    request.body = xml_req
    
    http = Net::HTTP.new(@uri.host, @uri.port)
    response = http.request(request)
 
    response.body   
    end 
  end
  
  # Read can get all ideas with no arguments or
  # get one idea with one argument.  For example:
  # api = Api.new
  # api.read 2 => one idea
  # api.read   => all ideas
  def read(id=nil)
    request = Net::HTTP.new(@uri.host, @uri.port)
 
    if id.nil?
      response = request.get("#{@uri.path}.xml")      
    else
      response = request.get("#{@uri.path}/#{id}.xml")    
    end
    
    response.body
  end
  
  # Update an idea using a predefined XML template as a REST request.
  def update(id=0,rootID=0,parentId=0,title="",description="",username="",tag="",rating=1)
    if id == 0 || title == "" || description == "" || username == "" || tag == ""
      return "Invalid params"
    else
    xml_req =
    "<?xml version='1.0' encoding='UTF-8'?>
    <IdeaNode>
      <id type='integer'>#{id}</id>
      <rootId type='integer'>#{rootId}</rootId>
      <parentId type='integer'>#{parentId}</parentId>
      <title>#{title}</title>
      <description>#{description}</description>
      <username>#{username}</username>
      <tag>#{tag}</tag>
      <rating type='integer'>#{rating}</rating>
    </IdeaNode>"
    
    request = Net::HTTP::Put.new("#{@url}/#{id}.xml")
    request.add_field "Content-Type", "application/xml"
    request.body = xml_req
    
    http = Net::HTTP.new(@uri.host, @uri.port)
    response = http.request(request)
    
    # no response body will be returned
    case response
    when Net::HTTPSuccess
      return "#{response.code} OK"
    else
      return "#{response.code} ERROR"
    end
  end
  end
  
  # Delete an idea with an ID using HTTP Delete
  def delete(id)
    request = Net::HTTP::Delete.new("#{@url}/#{id}.xml")
    http = Net::HTTP.new(@uri.host, @uri.port)
    response = http.request(request)
    
    # no response body will be returned
    case response
    when Net::HTTPSuccess
      return "#{response.code} OK"
    else
      return "#{response.code} ERROR"
    end
  end
    
end
