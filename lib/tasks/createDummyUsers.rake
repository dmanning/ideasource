namespace :db do
  namespace :development do
    desc "Create user records for the test user database."
    
    task :fake_user_data => :environment do
      require 'faker'
      
      10.times do
      	
        user = User.new(
          :username => Faker::Internet.user_name,
          :password => "greatpasswordhuh",
          :password_confirmation => "greatpasswordhuh",
          :email => Faker::Internet.email
          )
        
        user.save!
        
      end
    end
  end
end
