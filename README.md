To access IdeaSource on a standalone machine
-----------------------------------------------

1. Run bundle install
2. Run rake db:migrate to populate the database
3. Run rails s to start the rails server
4. Open a browser and go to localhost:3000 to access the main page of IdeaSource

To access documentation
----------------------------------------------- 

to get to the documentation go to and open doc/app/index.html
this will provide you with a browsable version of the app
documentation

Browsers this application will work on
-----------------------------------------------

Firefox 4+  

Chrome 10+  

Safari 5.1+  

Opera 11.64+  

Internet Explorer 9+  


