module IdeaNodesHelper
	def validateIdea(idea)
		if idea.title == ""
			return false
		end
		if idea.description == ""
			return false
		end
		if idea.tag == ""
			return false;
		end
		return true
	end
end
