module MindmapHelper

	def getNodes(rootId)
		root = IdeaNode.find(rootId)
		list = content_tag(:ul) do
			content_tag(:li) do
				tmp = content_tag(:a, root.title, :id => root.id)
				IdeaNode.all.each do |node|
					if node.parentId == root.id
						tmp = tmp + getNodes(node.id)
					end 
				end
				tmp
			end
		end.html_safe
		return list
	end
end
