class MindmapController < ActionController::Base
  protect_from_forgery
<<<<<<< HEAD
  layout "application"
=======
>>>>>>> 0852a360a9903c058cf3a12e99f77c7bd48c0882
  before_filter :authenticate_user! 
  
  def map
  	@idea_node = IdeaNode.all
  	@root = IdeaNode.find(params[:id])
  end
  def discover
    if params[:search_value] != nil
      @tags = Tag.where('name LIKE ?','%'+params[:search_value]+'%')
    elsif params[:search_value] == ""
      @tags = Tag.all
    else
      @tags = Tag.all
    end
  end

  def search
    @tag = params[:tag]
    if params[:sort_by] == "None"
      order_value = ""
    elsif params[:sort_by] == "Newest"
      order_value = "created_at desc"
    elsif params[:sort_by] == "Oldest"
      order_value = "created_at asc"
    elsif params[:sort_by] == "User"
      order_value = "username"
    elsif params[:sort_by] == "Rating"
      order_value = "rating desc"
    end
    if params[:search_value] != nil
      @ideas = IdeaNode.where("rootId = 0 AND tag = '"+params[:tag]+"' AND title LIKE ?","%"+params[:search_value]+"%").order(order_value)
    elsif params[:search_value] == ""
      @ideas = IdeaNode.where(:tag => params[:tag],:rootId => 0).order(order_value)
    else
      @ideas = IdeaNode.where(:tag => params[:tag],:rootId => 0).order(order_value)
    end
  end
end
