class IdeaNodesController < ApplicationController
  include IdeaNodesHelper
<<<<<<< HEAD
  layout "application"
=======
  layout "mindmap"
>>>>>>> 0852a360a9903c058cf3a12e99f77c7bd48c0882
  # GET /idea_nodes
  # GET /idea_nodes.xml
  # GET /idea_nodes.json
  def index
    @idea_nodes = IdeaNode.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @idea_nodes }
      format.json { render :json => @idea_nodes }
    end
  end

  # GET /idea_nodes/1
  # GET /idea_nodes/1.xml
  # GET /idea_nodes/1.json
  def show
    @idea_node = IdeaNode.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @idea_node }
      format.json { render :json => @idea_node }
    end
  end

  # GET /idea_nodes/new
  # GET /idea_nodes/new.xml
  # GET /idea_nodes/new.json
  def new
    @idea_node = IdeaNode.new
    @root = params[:rootId]
    @parent = params[:parentId]

    respond_to do |format|
      format.html 
      format.xml  { render :xml => @idea_node }
      format.json { render :json => @idea_node }
    end
  end

  # GET /idea_nodes/1/edit
  def edit
    @idea_node = IdeaNode.find(params[:id])
  end

  # POST /idea_nodes
  # POST /idea_nodes.xml
  # POST /idea_nodes.json
  def create
    @idea_node = IdeaNode.new(params[:idea_node])
    check  = Tag.count(:conditions => {:name => @idea_node.tag})
    if check == 0
      Tag.create(:name => @idea_node.tag)
    end
    #desides where to go
    id = @idea_node.rootId

    respond_to do |format|

      if validateIdea(@idea_node)
      if @idea_node.save
        if id == 0
          id = @idea_node.id
        end
        format.html { redirect_to :controller => "mindmap", :action => "map",:id => id }
        format.xml { render :xml => @idea_node, :status => :created, :location => @idea_node }
        format.json { render :json => @idea_node, :status => :created, :location => @idea_node }
      else
        format.html { render :action => "new" }
        format.json { render :json => @idea_node.errors, :status => :unprocessable_entity }
        format.xml { render :xml => @idea_node.errors, :status => :unprocessable_entity }
      end
    else
        format.html { render :action => "new", :rootId => @idea_node.rootId, :parentId => @idea_node.parentId }
        format.json { render :json => @idea_node.errors, :status => :unprocessable_entity }
        format.xml { render :xml => @idea_node.errors, :status => :unprocessable_entity }
    end
    end
  end

  # PUT /idea_nodes/1
  # PUT /idea_nodes/1.xml
  # PUT /idea_nodes/1.json
  def update
    @idea_node = IdeaNode.find(params[:id])

    respond_to do |format|
      if @idea_node.update_attributes(params[:idea_node])
        format.html { redirect_to @idea_node, :notice => 'Idea node was successfully updated.' }
        format.json { head :no_content }
        format.xml { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @idea_node.errors, :status => :unprocessable_entity }
        format.xml { render :xml => @idea_node.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /idea_nodes/1
  # DELETE /idea_nodes/1.xml
  # DELETE /idea_nodes/1.json
  def destroy
    @idea_node = IdeaNode.find(params[:id])
    count = IdeaNode.where(:tag => @idea_node.tag).count
    if count == 1
      Tags.where(:name => @idea_node.tag).destroy
    end
    @idea_node.destroy

    respond_to do |format|
      format.html { redirect_to idea_nodes_url }
      format.json { head :no_content }
      format.json { head :no_content }
    end
  end
end
