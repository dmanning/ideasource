// load the mindmap
$(document).ready(function() {
  buildMap();
});

$(window).resize(function(){
    location.reload();
});

<<<<<<< HEAD


=======
>>>>>>> 0852a360a9903c058cf3a12e99f77c7bd48c0882
function positionMindMap(){
    var position = $("#content").position();
    $("svg").css("width",$("#content").innerWidth());
    $("svg").css("height",$("#content").innerHeight());
    $("svg").css("top",position.top+10);
<<<<<<< HEAD
    $("svg").css("left",position.left+40);
=======
    $("svg").css("left",position.left);
>>>>>>> 0852a360a9903c058cf3a12e99f77c7bd48c0882

    $("#MindMapDiv").css("position","absolute");
    $("#MindMapDiv").css("width",$("#content").innerWidth());
    $("#MindMapDiv").css("height",$("#content").innerHeight());
    $("#MindMapDiv").css("top",position.top+10);
<<<<<<< HEAD
    $("#MindMapDiv").css("left",position.left+40);

}

  function codeLatLng(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        //find country name
        city = results[0].address_components[3];
        country = results[0].address_components[5];
        $('#location').text(city.long_name+", "+country.long_name);
    }
    });
  }

=======
    $("#MindMapDiv").css("left",position.left);

}

>>>>>>> 0852a360a9903c058cf3a12e99f77c7bd48c0882
function displayInfo(nodeId){
  $('#popup-bg').css('display','block');
  $('#popup').css('display','block');
  var link = $('#newIdeaLink').attr('href')+"/"+nodeId;
  $('#newIdeaLink').attr('href',link);
  getInfo(nodeId);

  $('#thumb_up').click(function(){thumb_up(nodeId);});
  $('#thumb_down').click(function(){thumb_down(nodeId);});
}
function getInfo(nodeId){
  $.getJSON("/idea_nodes/"+nodeId,function(data){
      node = data;
      $('#title').text(data.title);
      $('#desc').text(data.description);
      $('#username').text(data.username);
      $('#created_at').text(data.created_at);
      $('#rating').text(data.rating);
<<<<<<< HEAD
     var location = codeLatLng(data.latitude,data.longitude);
      $('#location').text(location);
=======
>>>>>>> 0852a360a9903c058cf3a12e99f77c7bd48c0882
  });
}
function thumb_up(nodeId){
    $.getJSON("/idea_nodes/"+nodeId,function(data){
      data.rating = (data.rating +1);
     $.ajax({
      url: "/idea_nodes/"+nodeId+".json",
      type: 'PUT',
      contentType: 'application/json',
      data: JSON.stringify(data),
      dataType: 'json'
      }).done(function(){getInfo(nodeId)});
    }); 
}
function thumb_down(nodeId){
    $.getJSON("/idea_nodes/"+nodeId,function(data){
      data.rating = (data.rating -1);
     $.ajax({
      url: "/idea_nodes/"+nodeId+".json",
      type: 'PUT',
      contentType: 'application/json',
      data: JSON.stringify(data),
      dataType: 'json'
      }).done(function(){getInfo(nodeId)});
    }); 
}
function hideInfo(){
  $('#popup-bg').css('display','none');
  $('#popup').css('display','none');
  var link = $('#newIdeaLink').attr('href')
  var index = link.lastIndexOf("/");
  link = link.substr(0,index);
  $('#newIdeaLink').attr('href',link);
}

function buildMap(){
  // enable the mindmap in the body
  $('#MindMapDiv').mindmap($("#content"));

  // add the data to the mindmap
  var root = $('#MindMapDiv>ul>li').get(0).mynode = $('#MindMapDiv').addRootNode($('#MindMapDiv>ul>li>a').text(), {
    nodeId: $('#MindMapDiv>ul>li>a').attr("id"),
    onclick:function(node) {
      $(node.obj.activeNode.content).each(function() {
        this.hide();
      });
      if(node.obj.activeNode == node){displayInfo(node.nodeId);}
    }
  });
  $('#MindMapDiv>ul>li').hide();
  var addLI = function() {
    var parentnode = $(this).parents('li').get(0);
    if (typeof(parentnode)=='undefined') parentnode=root;
      else parentnode=parentnode.mynode;
    
    this.mynode = $('#MindMapDiv').addNode(parentnode, $('a:eq(0)',this).text(), {
      nodeId: $('a:eq(0)',this).attr("id"),
      onclick:function(node) {
        $(node.obj.activeNode.content).each(function() {
          this.hide();
        });
        $(node.content).each(function() {
          this.show();
        });
        //displays node info
        if(node.obj.activeNode == node){displayInfo(node.nodeId);}
      }
    });
    $(this).hide();
    $('>ul>li', this).each(addLI);
  };
  $('#MindMapDiv>ul>li>ul').each(function() { 
    $('>li', this).each(addLI);
  });
  positionMindMap();
}
