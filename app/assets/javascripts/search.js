$(document).ready(function(){
	$('#sort').change(function(){
		search();
	});
});
function search(){
	var search_value = $('#searchBar').val();
	window.location = "/search/"+$('#tag').val()+"/"+getSort()+"/"+search_value;
}
function getSort(){
	var sort_field = document.getElementById('sort');
	var value = sort_field.options[sort_field.selectedIndex].text; 
	return value;
}