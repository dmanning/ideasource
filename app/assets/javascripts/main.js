$(document).ready(function(){
	var pageName = $('#pageName').val();
	if(pageName == "home"){
		$('#home').addClass('active');
	}else if(pageName == "about"){
		$('#about').addClass('active');
	}else if(pageName == "discover"){
		$('#discover').addClass('active');
	}else if(pageName == "userHome"){
		$('#userHome').addClass('active');
	}
});
function signOut(){
	$.ajax({
      url: "/users/sign_out",
      type: 'DELETE',
      success: function(){window.location.href = "/";}
  	});
}